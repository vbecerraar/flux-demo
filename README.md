# flux-demo

lab to test flux

## Getting started

Establish a Terminal Session on the Kubernetes Master and Install Flux
Use SSH to establish a session on the Kubernetes host server:

ssh cloud_user@[IP Address Here]
Use the password supplied in the lab interface.

To verify that fluxctl was installed automatically, type:

$ fluxctl version
If it was not installed automatically, you may enter:

$ sudo snap install fluxctl --classic
Once on the server, create the namespace to run Flux:

$ kubectl create namespace flux
Then set the GLUSER environment variable:

$ export GLUSER=[your GitLab username]
Then input the command to run flux:

$ fluxctl install \
--git-user=${GLUSER} \
--git-email=${GLUSER}@gmail.com \
--git-url=git@gitlab.com:${GLUSER}/flux-demo \
--git-path=namespaces,workloads \
--namespace=flux | kubectl apply -f -
You may check the deployment with the following command:

$ kubectl -n flux rollout status deployment/flux

Obtain the RSA Key Created by fluxctl, and Grant GitLab Write Permission to the Cluster
Obtain the RSA key created by the Flux install procedure with the following command:

$ fluxctl identity --k8s-fwd-ns flux

#By default flux do the sync automaticatly every 5 minutes.
#But, to test, u can force manual sync
Use the fluxctl sync Command to Synchronize the Cluster
After the GitLab account has been granted write access to the Cluster, use the sync command to apply the YAML from the repository:

$ fluxctl sync --k8s-fwd-ns flux
